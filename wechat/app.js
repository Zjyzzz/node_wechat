'use strict'
var Koa = require('koa');
var path = require('path');
var wechat = require("./wechat/g.js");
var util = require('./libs/util')
var wechat_files = path.join(__dirname, "./config/wechat.txt");

var config = {
    wechat: {
        appID: 'wx1681f512f8070edc',
        appSecret: 'e15ff54397ad0831d8d95197ac5225e7',
        token: 'zjynodejsmooctoken',
        getAccessToken: function () {
            return util.readFileAsync(wechat_files)
        },
        saveAccessToken: function (data) {
            data = JSON.stringify(data)
            return util.writeFileAsync(wechat_files, data)
        }
    }
}

var app = new Koa();
// 迭代器函数
app.use(wechat(config.wechat))

app.listen(1234)

console.log("server is running");